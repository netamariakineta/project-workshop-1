CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	users_name VARCHAR(100),
	users_username VARCHAR (100),
    address VARCHAR (255),
    phone VARCHAR (20),
    email VARCHAR (100),
    UNIQUE KEY users_unique(users_username, email)
)