module workshop-trial1

go 1.19

require (
	github.com/go-sql-driver/mysql v1.7.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.2 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
