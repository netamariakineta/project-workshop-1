package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/go-playground/validator.v9"
	_ "gopkg.in/go-playground/validator.v9"
)

func main() {
	fmt.Println("SERVICE WORKSHOP STARTED ON PORT: 8088")
	http.HandleFunc("/getlist", GETHandler)
	http.HandleFunc("/add", POSTHandler)
	http.HandleFunc("/update", UpdateUsers)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

const (
	username = "root"
	password = ""
	hostname = "127.0.0.1:3306"
	dbname   = "workshop1"
)

// func dsn(dbName string) string {
// 	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
// }

type Users struct {
	Id            int    `json:"id"`
	UsersName     string `json:"usersName"`
	UsersUsername string `json:"usersUsername"`
	Address       string `json:"address"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
}
type RequestAddUser struct {
	UsersName     string `json:"usersName" validate:"required"`
	UsersUsername string `json:"usersUsername" validate:"required"`
	Address       string `json:"address"`
	Phone         string `json:"phone"`
	Email         string `json:"email" validate:"required,email"`
}
type RequestUpdateUser struct {
	Id            int    `json:"id" validate:"required"`
	UsersName     string `json:"usersName"`
	UsersUsername string `json:"usersUsername"`
	Address       string `json:"address"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
}

type Response struct {
	ErrMsg string      `json:"errMsg"`
	Status bool        `json:"status"`
	Result interface{} `json:"result"`
}

func OpenConnection() *sql.DB {
	db, err := sql.Open("mysql", username+":"+password+"@tcp("+hostname+")/"+dbname)

	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return db
}

// view
func GETHandler(w http.ResponseWriter, r *http.Request) {
	var resUser []Users
	db := OpenConnection()
	rows, err := db.Query("SELECT id, users_name, users_username, address, phone, email FROM users")
	if err != nil {
		log.Fatal(err)
		response := Response{
			ErrMsg: "FAILED GET USER LIST " + err.Error(),
			Status: true,
			Result: resUser,
		}
		empBytes, err := json.MarshalIndent(response, "", "\t")
		if err != nil {
			log.Println("ERROR MARSHALLING LIST USER RESPONSE: ", err.Error())
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(empBytes)
	}
	for rows.Next() {
		var scan Users
		rows.Scan(&scan.Id, &scan.UsersName, &scan.UsersUsername, &scan.Address, &scan.Phone, &scan.Email)
		resUser = append(resUser, scan)
	}
	response := Response{
		ErrMsg: "SUCCESS GET USER LIST",
		Status: true,
		Result: resUser,
	}
	fmt.Println("RES USER: ", resUser)
	empBytes, err := json.MarshalIndent(response, "", "\t")
	// fmt.Println(err)
	if err != nil {
		response.ErrMsg = "ERROR MARSHALLING RESULT"
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(empBytes)
	defer rows.Close()
	defer db.Close()
}

// insert
func POSTHandler(w http.ResponseWriter, r *http.Request) {
	var request RequestAddUser
	var idUser int
	db := OpenConnection()
	validate := validator.New()

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := validate.Struct(&request); err != nil {
		log.Println("ERROR VALIDATE STRUCT", err.Error())
		response := Response{
			ErrMsg: "FAILED ADD USER",
			Status: false,
			Result: err.Error(),
		}
		log.Println(response)
		empBytes, err := json.MarshalIndent(response, "", "\t")
		// fmt.Println(err)
		if err != nil {
			response.ErrMsg = "ERROR MARSHALLING RESULT"
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(empBytes)
		return
	}
	fmt.Println(request)
	query := `INSERT INTO users(users_name, users_username,address, phone, email) VALUES (?, ?, ?, ?, ?)`
	// query = ReplaceSQL(query, "?")

	_, err = db.Query(query, request.UsersName, request.UsersUsername, request.Address, request.Phone, request.Email)
	fmt.Println(query)
	if err != nil {
		log.Println("ERROR ADD USER: ", err.Error())
		response := Response{
			ErrMsg: "FAILED ADD USER",
			Status: false,
			Result: err.Error(),
		}
		log.Println(response)
		empBytes, err := json.MarshalIndent(response, "", "\t")
		// fmt.Println(err)
		if err != nil {
			response.ErrMsg = "ERROR MARSHALLING RESULT"
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(empBytes)
		return
	}
	fmt.Println("SUCCESS ADD USER: ", idUser)
	response := Response{
		ErrMsg: "SUCCESS ADD USER LIST",
		Status: true,
		Result: nil,
	}
	empBytes, err := json.MarshalIndent(response, "", "\t")
	fmt.Println("ID USER>> ", idUser)
	if err != nil {
		response.ErrMsg = "ERROR MARSHALLING RESULT"
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(empBytes)

}

func UpdateUsers(w http.ResponseWriter, r *http.Request) {
	var request RequestUpdateUser
	var resUser Users
	db := OpenConnection()
	validate := validator.New()

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := validate.Struct(&request); err != nil {
		log.Println("ERROR VALIDATE STRUCT", err.Error())
		return
	}
	fmt.Println(request)
	query := `UPDATE users SET users_name=?,users_username=?,address=?,phone=?,email=? WHERE id=?`
	// query = ReplaceSQL(query, "?")

	_, err = db.Query(query, request.UsersName, request.UsersUsername, request.Address, request.Phone, request.Email, request.Id)
	fmt.Println(query)
	if err != nil {
		log.Println("ERROR UPDATE USER: ", err.Error())
		response := Response{
			ErrMsg: "FAILED UPDATE USER",
			Status: false,
			Result: err.Error(),
		}
		log.Println(response)
		empBytes, err := json.MarshalIndent(response, "", "\t")
		// fmt.Println(err)
		if err != nil {
			response.ErrMsg = "ERROR MARSHALLING RESULT"
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(empBytes)
		return
	}
	query = `SELECT id, users_name, users_username,address, phone, email FROM users WHERE id=?`
	err = db.QueryRow(query, request.Id).Scan(&resUser.Id, &resUser.UsersName, &resUser.UsersUsername, &resUser.Address,
		&resUser.Phone, &resUser.Email)
	if err != nil {
		log.Println("error querying get users update", err)
	}
	fmt.Println("SUCCESS UPDATE USER: ", resUser)
	response := Response{
		ErrMsg: "SUCCESS UPDATE USER LIST",
		Status: true,
		Result: resUser,
	}
	empBytes, err := json.MarshalIndent(response, "", "\t")
	fmt.Println("ID USER>> ", resUser)
	if err != nil {
		response.ErrMsg = "ERROR MARSHALLING RESULT"
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(empBytes)

}

func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}
